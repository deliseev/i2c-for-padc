----------------------------------------------------------------------------------
-- Company: RWTH Aachen University
-- Engineer: Dmitry Eliseev
-- 
-- Create Date: 29.10.2019 17:49:18
-- Design Name: 
-- Module Name: padc_interface - Behavioral
-- Project Name: i2c wrapper for PADC<->FPGA interconnection
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.padc_regs_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity padc_interface is
    GENERIC(
        top_clk_freq : INTEGER := 40_000_000;   --input clock speed from user logic in Hz
        i2_clk_freq  : INTEGER := 1_000;       --speed the i2c bus (scl) will run at in Hz
        PADC_polling_freq  : INTEGER := 2);     -- How many times pro second the i2c polling is provided. Possible values: 1x, 2x ,3x, 4x
    Port( 
        sysclk              : in STD_LOGIC;
        reset_n             : in STD_LOGIC;
        debug_out1          : out std_logic;
        debug_out2          : out std_logic;
        debug_out3          : out std_logic;
        debug_out4          : out std_logic;
        padc_regs           : out t_padc_regs;
        padc_control_reg    : in std_logic_vector;
        i2c_sda             : inout STD_LOGIC;            --serial data output of i2c bus
        i2c_scl             : inout STD_LOGIC             --serial clock output of i2c bus
        );
end padc_interface;

architecture Behavioral of padc_interface is

component i2c_master is
  GENERIC(
    input_clk : INTEGER := top_clk_freq; --input clock speed from user logic in Hz
    bus_clk   : INTEGER := i2_clk_freq);   --speed the i2c bus (scl) will run at in Hz
  PORT(
    clk       : IN     STD_LOGIC;                    --system clock
    reset_n   : IN     STD_LOGIC;                    --active low reset
    ena       : IN     STD_LOGIC;                    --latch in command
    addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
    rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
    data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
    busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
    data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
    ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
    sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
    scl       : INOUT  STD_LOGIC
    );                   --serial clock output of i2c bus
END component;

CONSTANT PADC_poll_delay  : INTEGER := (top_clk_freq/PADC_polling_freq); 
signal PADC_delay_counter  : std_logic_vector (31 downto 0);
signal PADC_readout_strobe  : std_logic;

signal PADC_polling_mode : std_logic;
signal PADC_start_config : std_logic;

signal i2c_reset_n          : std_logic;

signal i2c_addr             : std_logic_vector (6 downto 0);
signal i2c_data_wr          : std_logic_vector (7 downto 0);
signal i2c_data_rd          : std_logic_vector (7 downto 0);
signal i2c_busy             : std_logic;
signal i2c_busy_flank_monitor : std_logic_vector (1 downto 0);
signal i2c_error            : std_logic := '0';

signal i2c_ack_error        : std_logic;
signal i2c_enable           : std_logic;
signal i2c_rw_op            : std_logic;
signal latch_adc_samples    : std_logic_vector (11 downto 0); --Bit0 ... Bit11 to latch ADC data from channel 0 to channel 11 respectively
signal byte_counter         : std_logic_vector (4 downto 0); 
signal max_byte_number      : std_logic_vector (4 downto 0);

signal ch0_reg              : std_logic_vector (9 downto 0);
signal ch1_reg              : std_logic_vector (9 downto 0);
signal ch2_reg              : std_logic_vector (9 downto 0);
signal ch3_reg              : std_logic_vector (9 downto 0);
signal ch4_reg              : std_logic_vector (9 downto 0);
signal ch5_reg              : std_logic_vector (9 downto 0);
signal ch6_reg              : std_logic_vector (9 downto 0);
signal ch7_reg              : std_logic_vector (9 downto 0);
signal ch8_reg              : std_logic_vector (9 downto 0);
signal ch9_reg              : std_logic_vector (9 downto 0);
signal ch10_reg             : std_logic_vector (9 downto 0);

constant highest_channel_no     : std_logic_vector (3 downto 0) := X"A";  -- ADC channel numbering. Starts from 0
constant MAX1138_addr           : std_logic_vector (6 downto 0) := "0110101"; -- I2C address of the ADC
constant MAX1138_setup_byte     : std_logic_vector (7 downto 0) := "10100010"; --bit map  Bit_7: Setup-byte; Bits_6-4: External reference; Bit_3: Internal clock; Bit_2: Unipolar; Bit_1: no reset action; Bit_0: reserved 
signal MAX1138_config_byte      : std_logic_vector (7 downto 0);
signal data_regs_refreshed      : std_logic;
signal config_done              : std_logic;


type state_type is ( rst_state, not_initialized, i2c_problem, idle_state, begin_setup_transaction,  wait_setup_transaction_complete, 
                                begin_config_transaction, wait_config_transaction_complete, config_complete,
                                begin_readout_transaction, hold_read, read_complete); 
signal state, next_state : state_type;



begin
    debug_out1   <= i2c_busy;
    debug_out2   <= i2c_ack_error;
    debug_out3   <= config_done;
    debug_out4   <= i2c_enable;
    
    MAX1138_config_byte <= "000"&highest_channel_no&"1"; -- Config Byte for PADC. Bit map:  Bit_7: Config-byte; Bits_6-5: scan mode from ch.0 up to max channel Bits_4-1 Max ch number is 10; Bit_0: Single-ended

    padc_regs.ch0       <= ch0_reg;
    padc_regs.ch1       <= ch1_reg;
    padc_regs.ch2       <= ch2_reg;
    padc_regs.ch3       <= ch3_reg;
    padc_regs.ch4       <= ch4_reg;
    padc_regs.ch5       <= ch5_reg;    
    padc_regs.ch6       <= ch6_reg;
    padc_regs.ch7       <= ch7_reg;
    padc_regs.ch8       <= ch8_reg;
    padc_regs.ch9       <= ch9_reg;
    padc_regs.ch10      <= ch10_reg;

    padc_regs.status(7) <= padc_control_reg(7) and padc_control_reg(6) and padc_control_reg(5) 
                           and  padc_control_reg(4) and padc_control_reg(3) and padc_control_reg(2);    -- recycle higher bits of the control register
    padc_regs.status(6 downto 4) <= (others => '0');
    padc_regs.status(3) <= i2c_error;
    padc_regs.status(2) <= data_regs_refreshed; 
    padc_regs.status(1) <= config_done; -- goes low if i2c acknowledgement error happens    
    padc_regs.status(0) <= PADC_polling_mode;       
    
    PADC_start_config <= padc_control_reg(1);
    PADC_polling_mode <= padc_control_reg(0);

    
    i2c_addr     <= MAX1138_addr; --always working with MAX1138 (the heart-component of the PADC)
    max_byte_number(4 downto 0) <= highest_channel_no&'1'; --max byte number is double of highest channel number

    PADC_readout_strobe <= '1' when PADC_delay_counter = PADC_poll_delay and PADC_polling_mode = '1' else '0';
    
PADC_POLL_COUNTER: process (sysclk, reset_n, PADC_delay_counter)
    begin
        if (sysclk'event and sysclk ='1') then
            if (reset_n = '0' or PADC_delay_counter = PADC_poll_delay) then
                PADC_delay_counter <= (others=>'0');
            else
                PADC_delay_counter <= PADC_delay_counter+1;
            end if;
        end if;
    end process;

I2C_BLOCK: i2c_master 
    port map (
        clk         => sysclk,
        reset_n     => i2c_reset_n,
        ena         => i2c_enable,
        addr        => i2c_addr,
        rw          => i2c_rw_op,                 -- '0'= write, '1'=read
        data_wr     => i2c_data_wr,
        busy        => i2c_busy,
        data_rd     => i2c_data_rd,         --data read from slave
        ack_error   => i2c_ack_error,       --flag if improper acknowledge from slave
        sda         => i2c_sda,             --serial data output of i2c bus
        scl         => i2c_scl              --serial clock output of i2c bus
     );
    
SYNC_PROC: process (sysclk, reset_n)
    begin
       if (sysclk'event and sysclk = '1') then
          if (reset_n = '0') then
             state <= rst_state;
          elsif (i2c_ack_error ='1') then 
             state <= i2c_problem;
          else
             state <= next_state;
          end if;
       end if;
    end process;
    
I2C_ERROR_FLAG: process (sysclk, reset_n, config_done)
    begin
        if (sysclk'event and sysclk = '1') then
            if (reset_n = '0' or config_done = '1') then  --reset the error flag only if config was provided again.
                i2c_error <='0';
            elsif (i2c_ack_error= '1') then 
                i2c_error <='1';
            end if;
        end if;
    end process;    
     
DATA_REGISTERS_MANAGEMENT: process (sysclk, state, i2c_busy, byte_counter)
    begin     
       if (sysclk'event and sysclk = '1') then
           if (reset_n = '0') then
               ch0_reg <= (others =>'0');
           elsif (state = hold_read  and i2c_busy ='0' and byte_counter <= max_byte_number) then  -- latch data when byte is transferred from slave to master and slave releases the bus
                if (unsigned(byte_counter) = 0) then
                    ch0_reg(9 downto 8) <= i2c_data_rd(1 downto 0);  
                elsif (unsigned(byte_counter) = 1) then  
                    ch0_reg(7 downto 0) <= i2c_data_rd;
                elsif (unsigned(byte_counter) = 2) then  
                    ch1_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 3) then  
                    ch1_reg(7 downto 0) <= i2c_data_rd;
                elsif (unsigned(byte_counter) = 4) then  
                    ch2_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 5) then  
                    ch2_reg(7 downto 0) <= i2c_data_rd;   
                elsif (unsigned(byte_counter) = 6) then  
                    ch3_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 7) then  
                    ch3_reg(7 downto 0) <= i2c_data_rd;   
                elsif (unsigned(byte_counter) = 8) then  
                    ch4_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 9) then  
                    ch4_reg(7 downto 0) <= i2c_data_rd; 
                elsif (unsigned(byte_counter) = 10) then  
                    ch5_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 11) then  
                    ch5_reg(7 downto 0) <= i2c_data_rd;          
                elsif (unsigned(byte_counter) = 12) then  
                    ch6_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 13) then  
                    ch6_reg(7 downto 0) <= i2c_data_rd;     
                elsif (unsigned(byte_counter) = 14) then  
                    ch7_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 15) then  
                    ch7_reg(7 downto 0) <= i2c_data_rd;   
                elsif (unsigned(byte_counter) = 16) then  
                    ch8_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 17) then  
                    ch8_reg(7 downto 0) <= i2c_data_rd; 
                elsif (unsigned(byte_counter) = 18) then  
                    ch9_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 19) then  
                    ch9_reg(7 downto 0) <= i2c_data_rd;          
                elsif (unsigned(byte_counter) = 20) then  
                    ch10_reg(9 downto 8) <= i2c_data_rd(1 downto 0);
                elsif (unsigned(byte_counter) = 21) then  
                    ch10_reg(7 downto 0) <= i2c_data_rd;                                                                                                                  
                end if;
           end if;
       end if;
    end process;

MONITOR_I2C_BUSY_FLANK: process (sysclk, i2c_busy)
    begin
        if (sysclk'event and sysclk ='1') then
            i2c_busy_flank_monitor(0) <= i2c_busy;
            i2c_busy_flank_monitor(1) <= i2c_busy_flank_monitor(0);
        end if;
    end process;
    
COUNT_SAMPLES: process (sysclk, state, i2c_busy_flank_monitor, byte_counter, max_byte_number, i2c_ack_error)
   begin     
      if (sysclk'event and sysclk = '1') then
        if (reset_n = '0' or state = idle_state or state = not_initialized) then
            byte_counter <= (others=>'0');      
        elsif state = hold_read and i2c_busy_flank_monitor="10" then -- falling edge of i2c_busy signal
            byte_counter <= byte_counter+1;
        end if;
      end if;
   end process;
        
OUTPUT_DECODE: process (state, MAX1138_config_byte, byte_counter, max_byte_number)
    begin
       if state = rst_state then
          i2c_reset_n <= '0';       
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          data_regs_refreshed <='0';
          config_done <= '0';
       elsif state = not_initialized then
          i2c_reset_n <= '1';       
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          data_regs_refreshed <='0';   
          config_done <= '0';
       elsif state = i2c_problem then
          i2c_reset_n <= '0';       -- reset i2c core, because acknowledgement error will go away either by trying a new transaction or by reset 
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          config_done <= '0';
          data_regs_refreshed <='0'; 
       elsif state = idle_state then
          i2c_reset_n <= '1';        
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          config_done <= '1';  
          data_regs_refreshed <='0';
       elsif state = begin_setup_transaction then
          i2c_reset_n <= '1';        
          i2c_data_wr <= MAX1138_setup_byte; 
          i2c_enable <= '1';     
          i2c_rw_op <= '0'; -- write operation   
          config_done <= '0';
          data_regs_refreshed <='0';
       elsif state = wait_setup_transaction_complete then
          i2c_reset_n <= '1';        
          i2c_data_wr <= MAX1138_setup_byte; 
          i2c_enable <= '0'; 
          i2c_rw_op <= '0'; -- write operation
          config_done <= '0';
          data_regs_refreshed <='0';
       elsif state =  begin_config_transaction then
          i2c_reset_n <= '1';        
          i2c_data_wr <= MAX1138_config_byte;
          i2c_enable <= '1';
          i2c_rw_op <= '0'; -- write operation      
          config_done <= '0';
          data_regs_refreshed <='0';
       elsif state = wait_config_transaction_complete then
          i2c_reset_n <= '1';        
          i2c_data_wr <= MAX1138_config_byte; 
          i2c_enable <= '0';     
          i2c_rw_op <= '0'; -- write operation   
          config_done <= '0';
          data_regs_refreshed <='0';
       elsif state = config_complete then 
          i2c_reset_n <= '1';       
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          config_done <= '1';         
          data_regs_refreshed <='0';
       elsif state= begin_readout_transaction then
          i2c_reset_n <= '1';        
          i2c_data_wr <= (others => '0');
          i2c_enable <= '1';     
          i2c_rw_op <= '1'; -- read operation  
          config_done <= '1'; 
          data_regs_refreshed <='0';
       elsif state = hold_read then
          i2c_reset_n <= '1';        
          if byte_counter<max_byte_number then
            i2c_enable <= '1';     
          else
            i2c_enable <= '0';
          end if;
          i2c_data_wr <= (others => '0');
          i2c_rw_op <= '1'; -- read operation  
          config_done <= '1';
          data_regs_refreshed <='0'; 
       elsif state = read_complete then
          i2c_reset_n <= '1';        
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0';
          config_done <= '1';
          data_regs_refreshed <='1';   
       else
          i2c_reset_n <= '1';       
          i2c_data_wr <= (others => '0');
          i2c_enable <= '0';
          i2c_rw_op <= '0'; -- write operation    
          config_done <= '0';
          data_regs_refreshed <='0';  
       end if;
    end process;
     
     
NEXT_STATE_DECODE: process (state, PADC_start_config, PADC_readout_strobe, i2c_busy, byte_counter, max_byte_number)
    begin
    next_state <= state;  --default is to stay in current state
    case (state) is
        when rst_state =>
            next_state <= not_initialized;
        when not_initialized =>            
            if PADC_start_config= '1' then
               next_state <= begin_setup_transaction;
            else
               next_state <= not_initialized;
            end if;
        when i2c_problem =>
            next_state <= not_initialized;
        when idle_state =>
            if PADC_start_config= '1' then
               next_state <= begin_setup_transaction;
            elsif PADC_readout_strobe = '1' then
                next_state <= begin_readout_transaction;
            end if;
        when begin_setup_transaction =>
            if i2c_busy = '0' then
                next_state <= begin_setup_transaction;
            else
                next_state <= wait_setup_transaction_complete;
            end if;
        when wait_setup_transaction_complete =>
            if i2c_busy = '1' then
                next_state <= wait_setup_transaction_complete;
            else
                next_state <= begin_config_transaction;
          end if;
        when begin_config_transaction =>
            if i2c_busy = '0' then
                next_state <= begin_config_transaction;
            else
                next_state <= wait_config_transaction_complete;
            end if;          
        when wait_config_transaction_complete =>
            if i2c_busy = '1' then
                next_state <= wait_config_transaction_complete;
            else
                next_state <= config_complete;
            end if;
        when config_complete =>
             next_state <= idle_state;
        when begin_readout_transaction =>
            if i2c_busy ='0' then
                next_state <= begin_readout_transaction;
            else 
                next_state <= hold_read;
            end if;
        when hold_read =>
            if byte_counter <= max_byte_number then
                next_state <= hold_read;
            else
                next_state <= read_complete;
            end if;
        when read_complete =>
            next_state <= idle_state;            
        when others =>
            next_state <= not_initialized;
       end case;
    end process;


end Behavioral;
