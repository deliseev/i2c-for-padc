----------------------------------------------------------------------------------
-- Company: RWTH Aachen university
-- Engineer: Dmitry Eliseev
-- 
-- Create Date: 18.11.2019 11:30:54
-- Design Name: 
-- Module Name: padc_regs - package file for PADC registers description
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package padc_regs_pkg is
    type t_padc_regs is record
        ch0 : std_logic_vector (9 downto 0);
        ch1 : std_logic_vector (9 downto 0);
        ch2 : std_logic_vector (9 downto 0);
        ch3 : std_logic_vector (9 downto 0);
        ch4 : std_logic_vector (9 downto 0);
        ch5 : std_logic_vector (9 downto 0);
        ch6 : std_logic_vector (9 downto 0);
        ch7 : std_logic_vector (9 downto 0);        
        ch8 : std_logic_vector (9 downto 0); 
        ch9 : std_logic_vector (9 downto 0);
        ch10 : std_logic_vector (9 downto 0);                                          
        status : std_logic_vector (7 downto 0); 
    end record t_padc_regs;

end package padc_regs_pkg;