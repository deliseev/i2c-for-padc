# Interface PADC via I2C

I2C module plus specialized wrapper for interfacing the MAX1138 ADC. 
Separate implementation of the I2C-interface is needed, since the working frequency of the I2C-PADC-interface is 1kHz.